import { API_ENTERPRISE_URL } from './config'

export const requestOTPForOrganizationConstraint = async (
  organizationCode: string,
  mobilePhoneNumber: string,
  anonymousId: string,
) => {
  const resp = await fetch(
    API_ENTERPRISE_URL + `request_otp_for_organization_constraint`,
    {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        organizationCode: organizationCode.replace('-', ''),
        mobilePhoneNumber,
        anonymousId,
      }),
    },
  )

  return resp.json()
}

export const getOrganizationConstraint = async (
  refUUID: string,
  otp: string,
) => {
  const resp = await fetch(API_ENTERPRISE_URL + `get_organization_constraint`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ refUUID, otp }),
  })

  return resp.json()
}

export const confirmConsent = async (
  refUUID: string,
  otp: string,
  constraints: string,
) => {
  const resp = await fetch(API_ENTERPRISE_URL + `confirm_consent`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ refUUID, otp, constraints }),
  })

  return resp.json()
}

export const recordStatus = async (
  authToken: string,
  anonymousId: string,
  location: string,
  riskLevel: string,
) => {
  const resp = await fetch(API_ENTERPRISE_URL + `record_status`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ authToken, anonymousId, location, riskLevel }),
  })

  return resp.json()
}
