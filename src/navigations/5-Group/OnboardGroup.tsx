import React, { useState, useMemo } from 'react'
import { COLORS, FONT_FAMILY, FONT_SIZES, FONT_BOLD } from '../../styles'
import { useNavigation } from 'react-navigation-hooks'
import { SafeAreaView } from 'react-native-safe-area-context'
import {
  StatusBar,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native'
import { TextInputMask } from 'react-native-masked-text'
import { PrimaryButton } from '../../components/Button'
import { Link } from '../../components/Base'
import { useResetTo } from '../../utils/navigation'
import { FormHeader } from '../../components/Form/FormHeader'
import I18n from '../../../i18n/i18n'

export const OnboardGroup = () => {
  const navigation = useNavigation()
  const [code, setCode] = useState('')
  const isValidEnterpriseCode = useMemo(
    () => code.replace(/-/g, '').match(/^[0-9]{4}$/),
    [code],
  )
  const resetTo = useResetTo()
  const onBack = navigation.getParam('onBack')
  const backIcon = navigation.getParam('backIcon')
  const onAddOrg = navigation.getParam('onAddOrg')
  const onPressSubmitButton = () => {
    navigation.navigate('GroupAuthPhone', {
      onBack: () => {
        navigation.pop()
      },
      otpRoute: 'GroupAuthOTP',
      backIcon: 'close',
      cancelText: 'ยกเลิก',
      onAddOrg: onAddOrg,
      orgCode: code,
    })
  }

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView style={{ flex: 1, width: '100%' }}>
        <StatusBar backgroundColor={COLORS.WHITE} barStyle="dark-content" />
        <FormHeader onBack={onBack} backIcon={backIcon}>
          <View style={styles.header}>
            <Text style={styles.title}>กรุณากรอกรหัสองค์กร</Text>
            <Text style={styles.subtitle}>สอบถามจากองค์กรที่สังกัด</Text>
          </View>
        </FormHeader>
        <View style={styles.content}>
          <View
            style={{
              backgroundColor: COLORS.WHITE,
              borderWidth: 1,
              borderColor: COLORS.GRAY_2,
              borderRadius: 4,
              height: 60,
              width: '100%',
              justifyContent: 'center',
            }}
          >
            <TextInputMask
              type="custom"
              options={{
                mask: '99-99',
              }}
              onChangeText={(text) => {
                setCode(text.trim())
              }}
              value={code}
              autoFocus
              placeholder="ใส่รหัสองค์กร 4 ตัว"
              maxLength={5}
              keyboardType={'phone-pad'}
              style={{
                textAlign: 'center',
                fontSize: FONT_SIZES[600],
                fontFamily: FONT_FAMILY,
                letterSpacing: 2,
              }}
            />
          </View>
        </View>
        <View style={styles.footer}>
          <PrimaryButton
            disabled={!isValidEnterpriseCode}
            title={I18n.t('next')}
            style={{ width: '100%' }}
            containerStyle={{ width: '100%' }}
            onPress={onPressSubmitButton}
          />
          <TouchableOpacity
            onPress={() => {
              resetTo({ routeName: 'MainApp' })
            }}
            style={{ marginTop: 8 }}
          >
            <Link
              style={{
                fontSize: FONT_SIZES[500],
                color: '#576675',
                textDecorationLine: 'underline',
              }}
            >
              ยกเลิก
            </Link>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: COLORS.WHITE },
  header: {
    alignItems: 'flex-start',
    marginBottom: 16,
    marginHorizontal: 24,
  },
  title: {
    fontFamily: FONT_BOLD,
    fontSize: FONT_SIZES[700],
    alignItems: 'center',
    color: COLORS.BLACK_1,
    textAlign: 'center',
  },
  subtitle: {
    fontFamily: FONT_FAMILY,
    fontSize: FONT_SIZES[600],
    lineHeight: 24,
    alignItems: 'center',
    color: COLORS.SECONDARY_DIM,
    textAlign: 'center',
  },
  errorText: {
    color: COLORS.RED,
  },
  content: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    padding: 24,
    backgroundColor: COLORS.LIGHT_BLUE,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: COLORS.BORDER_LIGHT_BLUE,
  },
  footer: {
    alignItems: 'center',
    marginTop: 24,
    marginBottom: 16,
    paddingHorizontal: 24,
  },
})
