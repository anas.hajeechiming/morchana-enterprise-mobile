import { createStackNavigator } from 'react-navigation'
import { OnboardGroup } from './OnboardGroup'
import { AuthPhone } from '../1-Auth/AuthPhone'
import { TermsAndConditions } from './TermsAndConditions'
import { GroupAuthOTP } from './GroupAuthOTP'
/*
  handle deeplink
  morchana://app/:appId
*/
export const GroupStack = createStackNavigator(
  {
    TermsAndConditions,
    OnboardGroup,
    GroupAuthPhone: AuthPhone,
    GroupAuthOTP,
  },
  {
    headerMode: 'none',
    mode: 'modal',
  },
)
