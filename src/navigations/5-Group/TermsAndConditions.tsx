import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import {
  StatusBar,
  View,
  Text,
  StyleSheet,
  ScrollView,
  Alert,
  TouchableOpacity,
} from 'react-native'
import { PrimaryButton } from '../../components/Button'
import { useNavigation } from 'react-navigation-hooks'
import { useHUD } from '../../HudView'
import { COLORS, FONT_FAMILY, FONT_BOLD, FONT_SIZES } from '../../styles'
import { useResetTo } from '../../utils/navigation'
import { normalize } from 'react-native-elements'
import { Link } from '../../components/Base'
import { FormHeader } from '../../components/Form/FormHeader'
import { confirmConsent } from '../../api-enterprise'
import { useApplicationState, applicationState } from '../../state/app-state'

import I18n from '../../../i18n/i18n'

export const TermsAndConditions = () => {
  const { showSpinner, hide } = useHUD()
  const navigation = useNavigation()
  const resetTo = useResetTo()
  const group = navigation.getParam('group')
  const groupName = navigation.getParam('group_name')
  const content = navigation.getParam('content')
  const refUUID = navigation.getParam('refUUID')
  const otp = navigation.getParam('otp')
  const onAddOrg = navigation.getParam('onAddOrg')
  const [data] = useApplicationState()
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={'white'} barStyle="dark-content" />
      <FormHeader backIcon="close">
        <View style={styles.header}>
          <Text style={styles.title}>{`Terms & Conditions`}</Text>
          <Text style={styles.subtitle}>
            {`สำหรับการใช้งาน Enterprise Service ร่วมกับ ${groupName}`}
          </Text>
        </View>
      </FormHeader>
      <View style={styles.content}>
        <ScrollView
          contentContainerStyle={{
            backgroundColor: 'white',
          }}
          style={{
            borderColor: COLORS.GRAY_2,
            borderWidth: 1,
            borderRadius: 4,
          }}
        >
          <View style={{ padding: 16 }}>
            <Text style={styles.agreement}>{content}</Text>
          </View>
        </ScrollView>
      </View>
      <View style={styles.footer}>
        <PrimaryButton
          title="ยอมรับข้อกำหนดในการใช้งาน"
          style={{ width: '100%' }}
          containerStyle={{ width: '100%' }}
          onPress={async () => {
            showSpinner()
            try {
              const resp = await confirmConsent(refUUID, otp, content)
              console.log(resp)
              hide()
              if (resp.code === 0) {
                applicationState.setData('isEnterpriseRegistered', true)
                navigation.pop()
                const newOrg = {
                  org_code: group,
                  org_name: groupName,
                  authToken: resp.data.authToken,
                }
                onAddOrg(newOrg)
              }
            } catch (err) {
              console.log(err)
              hide()
              Alert.alert(I18n.t('error'))
            }
          }}
        />
        <TouchableOpacity
          onPress={() => {
            resetTo({ routeName: 'MainApp' })
          }}
          style={{ marginTop: 8 }}
        >
          <Link
            style={{
              fontSize: FONT_SIZES[500],
              color: '#576675',
              textDecorationLine: 'underline',
            }}
          >
            ปฎิเสธ
          </Link>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}

const padding = normalize(16)

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: 'white' },
  header: {
    alignItems: 'flex-start',
    marginBottom: 16,
    marginHorizontal: padding,
  },

  title: {
    fontFamily: FONT_BOLD,
    fontSize: FONT_SIZES[700],
    alignItems: 'center',
    color: COLORS.BLACK_1,
    textAlign: 'center',
  },

  subtitle: {
    fontFamily: FONT_FAMILY,
    fontSize: FONT_SIZES[500],
    lineHeight: 24,
    alignItems: 'center',
    color: COLORS.SECONDARY_DIM,
    textAlign: 'center',
  },
  content: {
    flex: 1,
    backgroundColor: COLORS.LIGHT_BLUE,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: COLORS.BORDER_LIGHT_BLUE,
    padding: padding,
  },
  agreement: {
    fontSize: FONT_SIZES[400],
    lineHeight: 24,
    color: COLORS.GRAY_4,
    marginBottom: 16,
    // textAlign: 'justify'
  },
  footer: {
    alignItems: 'center',
    paddingHorizontal: padding,
    marginBottom: 16,
  },
})
