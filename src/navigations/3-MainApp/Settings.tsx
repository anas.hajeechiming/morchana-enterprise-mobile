import React, { useState } from 'react'
import {
  StatusBar,
  StyleSheet,
  View,
  Text,
  Switch,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  Alert,
} from 'react-native'
import _ from 'lodash'
import { COLORS, FONT_FAMILY, FONT_SIZES } from '../../styles'
import { MyBackground } from '../../components/MyBackground'
import { SafeAreaView } from 'react-native-safe-area-context'
import { useContactTracer } from '../../services/contact-tracing-provider'
import { useNavigation } from 'react-navigation-hooks'
import { userPrivateData } from '../../state/userPrivateData'
import { useApplicationState, applicationState } from '../../state/app-state'
import I18n from '../../../i18n/i18n'

export const Settings = () => {
  const navigation = useNavigation()
  const { enable, disable, isServiceEnabled } = useContactTracer()
  const isRegistered = Boolean(userPrivateData.getData('authToken'))
  const [data] = useApplicationState()
  const [registeredOrgs, setOrgs] = useState<string[]>(
    data.registeredOrgs || [],
  )

  const _onPrivacyPolicyClicked = () => {
    navigation.navigate('PrivacyPolicy')
  }

  const onPressAddGroup = () => {
    navigation.navigate('OnboardGroup', {
      onAddOrg: (newOrg: any) => {
        const oldOrg = data.registeredOrgs || []
        const newRegisteredOrgs: any = [...oldOrg, newOrg]
        setOrgs(newRegisteredOrgs)
        applicationState.setData('registeredOrgs', newRegisteredOrgs)
      }
    })
  }

  const onPressRemoveOrg = (orgToRemove: string) => {
    Alert.alert('ยืนยันการออกจากองค์กร', '', [
      {
        text: 'ออก',
        onPress: () => {
          setOrgs((oldOrgs) => {
            const newOrgs = _(oldOrgs)
              .filter((org) => {
                if (orgToRemove === org.org_code) {
                  return undefined
                }
                return org
              })
              .compact()
              .value()
            console.log('kenod jaa eidi', newOrgs)
            applicationState.setData('registeredOrgs', false)
            return newOrgs
          })
        },
      },
      {
        text: 'อยู่ต่อ',
        onPress: _.noop,
      },
    ])
  }

  const renderSystemSection = () => {
    return (
      <>
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>{I18n.t('tracking')}</Text>
        </View>
        <View style={styles.settingsSection}>
          <View style={[styles.section]}>
            <View style={styles.horizontalRow}>
              <View style={styles.leftArea}>
                <Text style={styles.sectionText}>
                  {I18n.t('track_with_bluetooth')}{' '}
                </Text>
              </View>
              <View style={styles.rightArea}>
                <Switch
                  trackColor={{
                    false: '#767577',
                    true: COLORS.PRIMARY_DARK,
                  }}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={() =>
                    isServiceEnabled ? disable() : enable()
                  }
                  value={isServiceEnabled}
                />
              </View>
            </View>
            <Text style={styles.sectionDescription}>
              {I18n.t('auto_turn_on_bluetooth_tracing')}
              {I18n.t('may_cause_phone_to_consume_higher_energy')}
              {I18n.t('you_can_choose_to_turn_off')}
              {I18n.t('but_sys_will_not_auto_trace')}
            </Text>
          </View>
        </View>
      </>
    )
  }

  const renderGeneralSection = () => {
    return (
      <>
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>{I18n.t('general')}</Text>
        </View>
        <View style={styles.settingsSection}>
          <TouchableHighlight onPress={_onPrivacyPolicyClicked}>
            <View style={styles.section}>
              <Text style={styles.sectionText}>{I18n.t('privacy_policy')}</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => navigation.navigate('Questionaire')}
          >
            <View style={styles.section}>
              <Text style={styles.sectionText}>
                {I18n.t('do_questionaire_again')}
              </Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => navigation.navigate('ChangeLanguage')}
          >
            <View style={styles.section}>
              <Text style={styles.sectionText}>{I18n.t('change_lang')}</Text>
            </View>
          </TouchableHighlight>
          {isRegistered && (
            <TouchableHighlight
              onPress={() =>
                navigation.navigate('OnboardPhone', {
                  onBack: () => {
                    navigation.pop()
                  },
                  backIcon: 'close',
                })
              }
            >
              <View style={styles.section}>
                <Text style={styles.sectionText}>
                  {I18n.t('identity_confirm')}
                </Text>
              </View>
            </TouchableHighlight>
          )}
        </View>
      </>
    )
  }

  const renderRegisteredOrg = (org: any, index: number) => {
    return (
      <>
        {index !== 0 && <View style={styles.line} />}
        <View style={styles.groupRow}>
          <Text>{org.org_name}</Text>
          <TouchableOpacity
            style={styles.closeButonContainer}
            onPress={() => onPressRemoveOrg(org.org_code)}
          >
            <Text style={styles.closeIcon}>x</Text>
          </TouchableOpacity>
        </View>
      </>
    )
  }

  const renderGroupsSection = () => {
    return (
      <>
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionHeaderText}>สำหรับองค์กร</Text>
        </View>
        <View style={styles.settingsSection}>
          <View style={[styles.section]}>
            <View style={styles.horizontalRow}>
              <View style={styles.leftArea}>
                <Text style={styles.sectionText}>
                  ส่งข้อมูลให้องค​์กรที่คุณสังกัด
                </Text>
              </View>
              <View />
            </View>
            <Text style={styles.sectionDescription}>
              ข้อมูลอยู่ภายใต้การดูแลขององค์กรที่คุณสังกัด
              หน่วยงานอื่นไม่สามารถเข้าถึงได้
              เพื่อเพิ่มประสิทธิภาพในการบริหารจัดการขององค์กรให้ดียิ่งขึ้น
            </Text>
            {registeredOrgs.map((org, index) =>
              renderRegisteredOrg(org, index),
            )}
            <TouchableOpacity
              style={styles.addGroupArea}
              onPress={onPressAddGroup}
            >
              <Text style={styles.addGroupText}>เพิ่มองค์กร +</Text>
            </TouchableOpacity>
          </View>
        </View>
      </>
    )
  }

  return (
    <MyBackground variant="light">
      <SafeAreaView style={{ flex: 1 }}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor={COLORS.PRIMARY_LIGHT}
        />
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}
        >
          <View>
            {renderSystemSection()}
            {renderGeneralSection()}
            {renderGroupsSection()}
          </View>
        </ScrollView>
      </SafeAreaView>
    </MyBackground>
  )
}

const styles = StyleSheet.create({
  section: {
    backgroundColor: '#ffffff',
    padding: 24,
    borderBottomWidth: 1,
    borderBottomColor: '#E0E0E0',
  },
  sectionHeader: {
    height: 56,
    justifyContent: 'flex-end',
    paddingLeft: 24,
    paddingRight: 24,
    paddingBottom: 8,
  },
  sectionHeaderText: {
    color: '#AAAAAA',
    fontSize: FONT_SIZES[600],
    fontFamily: FONT_FAMILY,
  },
  settingsSection: {
    borderTopWidth: 1,
    borderTopColor: '#E0E0E0',
  },
  horizontalRow: {
    flexDirection: 'row',
  },
  leftArea: {
    flex: 1,
  },
  rightArea: {
    justifyContent: 'flex-start',
  },
  sectionText: {
    fontSize: FONT_SIZES[600],
    color: '#000000',
    fontFamily: FONT_FAMILY,
  },
  sectionDescription: {
    marginTop: 4,
    fontSize: FONT_SIZES[500],
    color: '#888888',
    fontFamily: FONT_FAMILY,
  },
  mediumText: {
    fontSize: FONT_SIZES[600],
    color: '#000000',
  },
  largeText: {
    fontSize: FONT_SIZES[700],
    color: '#000000',
    fontFamily: FONT_FAMILY,
  },
  sectionTitle: {
    fontSize: FONT_SIZES[700],
    fontWeight: '600',
    color: '#000000',
    fontFamily: FONT_FAMILY,
  },
  scrollView: {},
  addGroupArea: {
    marginTop: 10,
    padding: 10,
    borderRadius: 20,
    borderColor: COLORS.GRAY_3,
    borderWidth: 1,
    alignItems: 'center',
  },
  addGroupText: {
    color: COLORS.GRAY_4,
  },
  groupRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  line: {
    height: 1,
    backgroundColor: COLORS.GRAY_1,
  },
  closeButonContainer: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: COLORS.GRAY_1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  closeIcon: {
    color: COLORS.GRAY_4,
  },
})
